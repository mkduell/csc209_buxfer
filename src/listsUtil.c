#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lists.h"
#include "listsUtil.h"

#ifdef DEBUG
#include "debug.h"
#endif


#define BUFFER_SIZE 256

/*
   Free the memory of the Group* passed in.
   Assuming all the values need to be freed.
 */
void freeGroup(Group* grp)
{
		if(grp!=NULL)
		{
				
				/*Free name */
				if(grp->name!=NULL)
				{
						free(grp->name);
						grp->name=NULL;
				}
				/*free users*/
				if(grp->users!=NULL)
				{
						User* user=grp->users;
						User* next;
						while(user!=NULL)
						{
								next=user->next;
								/* On the last iteration, the following should
								   be true:
								   user->next = next = NULL
								 */
								freeUser(user);
								user=next;
						}
						grp->users=NULL;
				}
				/*free xcts*/
				if(grp->xcts!=NULL)
				{
					Xct* xct=grp->xcts;
					Xct* nextXct;
					while(xct!=NULL)
					{
						nextXct=xct->next;
						freeXct(xct);
						xct=nextXct;
					}
				}
				free(grp);
				grp=NULL;
		}	
}

/*
   Free the memory of the User* passed in.
   Assuming all the values need to be freed.
 */
void freeUser(User* usr)
{

#ifdef DEBUG
		debug("Start:freeUser\n");
#endif
		if(usr!=NULL)
		{
				if(usr->name!=NULL)
				{
						free(usr->name);
						usr->name=NULL;
				}
				free(usr);
				usr=NULL;
		}
#ifdef DEBUG
		debug("End:freeUser\n");
#endif
}

/*
   Helper function to extend malloc. Automatically
   check for a successful malloc and exit if necessary.
 */
void* myMalloc(size_t size)
{
		void* mem = malloc(size);
		if(mem ==NULL)
		{
				perror("Unable to allocate memory.");
				exit(-1);
		}
		return mem;
}

Group* createGroup(const char* name)
{

#ifdef DEBUG
		char* debugMsg =malloc((strlen(name)+20));
		strncpy(debugMsg,"Name=",strlen(name)+20);  
		debug("Start:createGroup");
		debug(strcat(debugMsg,name));

#endif

		Group* grp=myMalloc(sizeof(Group));

		/*
		   limit the length of the string, try and
		   only use what is neccessary.

		   on the off chance that name isn't null terminated,
		   (resulting in strlen returning a length which isn't 
		   correct), give an arbitrary upper limit, BUFFER_SIZE 
		 */
		int limit = strlen(name);
		limit=(limit>BUFFER_SIZE)?BUFFER_SIZE:limit;

		grp->name=myMalloc(limit*sizeof(char*));
		/*add one to ensure null byte is automatically added*/
		strncpy(grp->name,name,limit+1);

		grp->next=NULL;
		grp->users=NULL;
		grp->xcts=NULL;	

#ifdef DEBUG
		debug("End:createGroup");
		strncpy(debugMsg,"Group->name=",strlen(name)+20);
		debug(strcat(debugMsg,grp->name));
#endif

		return grp;
}

User* createUser(const char* name)
{
#ifdef DEBUG
		char* debugMsg = (char*)malloc((strlen(name)+20));
		strncpy(debugMsg,"Name=",strlen(name)+20);  
		debug("Start:createUser");
		debug(strcat(debugMsg,name));

#endif

		User* usr = (User*)myMalloc(sizeof(User));
		int limit = strlen(name);
		limit=(limit>BUFFER_SIZE)?BUFFER_SIZE:limit;
		usr->name=(char*)myMalloc(limit*sizeof(char*));
		strncpy(usr->name,name,limit+1);

		usr->next=NULL;
		usr->balance=0;

#ifdef DEBUG
		debug("End:createUser");
		strncpy(debugMsg,"User->name=",strlen(name)+20);
		debug(strcat(debugMsg,usr->name));
#endif

		return usr;
}

Xct* createXct(const char* user_name,double amount)
{
	#ifdef DEBUG
		debug("Start:createXct\n");
	#endif

	Xct* xct = myMalloc(sizeof(Xct));
	xct->amount=amount;

	int limit=strlen(user_name);
	limit=(limit>BUFFER_SIZE)?BUFFER_SIZE:limit;
	xct->name=myMalloc(limit*sizeof(char*));
	strncpy(xct->name,user_name,limit+1);

	xct->next=NULL;

	#ifdef DEBUG
		debug("End:createXct\n");
	#endif
	return xct;
}
void freeXct(Xct* xct)
{
	#ifdef DEBUG
		debug("start:freeXct\n");
	#endif


	if(xct!=NULL)
	{
		if(xct->name!=NULL)
		{
			free(xct->name);
		}
		free(xct);
	}


	#ifdef DEBUG
		debug("End:freeXct\n");
	#endif
			
}
