#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lists.h"
#include "listsUtil.h"

int assertEqual(const char* s1,const char* s2,const char* msg)
{
	if(strcmp(s1,s2)==0)
	{
		return 0;
	}
	else
	{
		fprintf(stdout,"%s\n",msg);		
		return -1;
	}
}

int assertNotNull(Group* obj,const char* msg)
{
		if(obj==NULL)
		{
				fprintf(stdout,"%s\n",msg);
				return -1;
		}
		else
		{
				return 0;
		}
}

int assertNull(Group* obj,const char* msg)
{
		if(obj==NULL)
		{
				return 0;
		}
		else
		{
				fprintf(stdout,"%s\n",msg);
				return -1;
		}
}

void test_list_groups1()
{
		Group* groups;
		/*	Group** groupList; 
		 */	/*allocate group pointer */
		Group* first;
		groups = (Group*) malloc(sizeof(Group));
		/*	groupList=&groups;	
		 */	first=groups;

		groups->name="Group One";

		groups->next=(Group*)malloc(sizeof(Group));
		groups=groups->next;
		groups->name="Group2";	
		/*	list_groups(*groupList);
		 */	list_groups(first);


}
int test_find_group1()
{
	Group* group;
	Group** grpList = (Group**)malloc(sizeof(Group*));

	group = (Group*) malloc(sizeof(Group));
	*grpList=group;

	group->name="GroupOne";
	add_group(grpList,"GroupTwo");	
	add_group(grpList,"GroupThree");	
	add_group(grpList,"GroupFour");	
	add_group(grpList,"GroupFive");	
	add_group(grpList,"GroupSix");	
	add_group(grpList,"GroupSeven");	

	/*
		Test:Look for an existing group
	*/
	printf("Looking for GroupTwo...\n");
	group=find_group(*grpList,"GroupTwo");	
	
	if(strcmp(group->name,"GroupTwo")!=0)
	{
		printf("Could not find GroupTwo! Test Failed.\n");
		return -1;
	}
	printf("Success: Found GroupTwo!\n");

	/*
		Test: Look for a group that doesn't exist
	*/
	printf("Looking for non-existant group Blablabla...\n");
	group=find_group(*grpList,"Blablabla");

	if(group!=NULL)
	{
		printf("Should not have been able to find group Blablabla.Test Failed\n");
		return -1;
	}
	printf("Could not find group Blablabla. Test passed.\n");

	return 0;
}

int test_add_group()
{

	/*
		Start with a null group_list_ptr
	*/
	Group* grpList=NULL;
	printf("Adding a group, group_list_ptr is null to start. This will initialize it implicitly.\n");
	if(add_group(&grpList,"GroupOne")!=0)
	{
		printf("Error Adding GroupOne. Test Failed\n");
		return -1;
	}

	printf("Lists currently contains:\n");
	list_groups(grpList);
	printf("********************\n");

	printf("adding a bunch of groups now..\n");
	if(add_group(&grpList,"GroupTwo")!=0)
	{
		printf("Error adding GroupTwo. Test Failed\n");
		return -1;
	}	
	if(add_group(&grpList,"GroupThree")!=0)	
	{
		printf("Error adding GroupThree. Test Failed\n");
		return -1;
	}	
	if(add_group(&grpList,"GroupFour")!=0)	
	{
		printf("Error adding GroupFour. Test Failed\n");
		return -1;
	}	
	
	if(add_group(&grpList,"GroupFive")!=0)	
	{
		printf("Error adding GroupFive. Test Failed\n");
		return -1;
	}	
	if(add_group(&grpList,"GroupSix")!=0)	
	{
		printf("Error adding GroupSix. Test Failed\n");
		return -1;
	}	
	if(add_group(&grpList,"GroupSeven")!=0)	
	{
		printf("Error adding GroupSeven. Test Failed\n");
		return -1;
	}	
	printf("finished adding A bunch of groups..\n");	

	printf("Lists currently contains:\n");
	list_groups(grpList);
	printf("********************\n");
	
	/*Perform the actual Test. Confirm that the latest entry was the last
		one added */
	printf("Testing that the first element in the list is \"GroupOne\"\n");
	if(strcmp(grpList->name,"GroupOne")!=0)
	{
		printf("First Group is not \"GroupOne\" as expected. Test Failed\n");
		return -1;	
	}

	return 0;
}

int testRemoveUser()
{
	
	int rVal=0;
	int retVal=0;
	/*First create a group*/
	Group* group= createGroup("GroupOne");
	/*Next add a bunch of users*/
	add_user(group,"Sammy");
	add_user(group,"Marina");
	add_user(group,"Meagan");
	add_user(group,"Celise");
	add_user(group,"Alexandra");
	/*List the users*/
	list_users(group);
	/*Count the users*/
	int count1=0;
	User* usr=group->users;
	while(usr!=NULL)
	{
		count1++;
		usr=usr->next;
	}

	/*Remove a user from the middle of the list*/
	fprintf(stdout,"Count of users=%d\n",count1);
	/*Remove a user in the middle*/
	fprintf(stdout,"Removing user Marina...\n");
	retVal=remove_user(group,"Marina");

	if(retVal!=0)
	{
		rVal=-1;
		printf("User Marina potentially not removed - non-zero return value\n");
	}

	/*count the users, verify that the count has gone down */
	int count2=0;
	usr=group->users;
	while(usr!=NULL)
	{
		count1++;
		usr=usr->next;
	}
	
	fprintf(stdout,"Count of users=%d\n",count2);
	if(count2==count1)
	{
		printf("User not removed, count is the same\n");
		rVal=-1;
	}
	else
	{
		fprintf(stdout,"User Marina Removed\n");
	}
	/*List the users to verify removal and correct order */
	list_users(group);




	
	/*
		Next Test: Removes user from the start of the list
	*/	
	printf("*****************\n");
	printf("Now testing removing from the Start\n");
	printf("Removing Alexandra...\n");
	remove_user(group,"Alexandra");
	if(retVal!=0)
	{
		rVal=-1;
		printf("User Alexandra potentially not removed - non-zero return value\n");
	}
	
	count1=0;
	usr=group->users;
	while(usr!=NULL)
	{
		count1++;
		usr=usr->next;
	}
	fprintf(stdout,"Count of users=%d\n",count1);
	if(count2==count1)
	{
		printf("User not removed, count is the same");
		rVal=-1;
	}
	else
	{
		printf("User Alexandra Removed\n");
	}
	/*List the users to verify removal and correct order */
	list_users(group);





	/*
		Next Test: Remove a user that doesn't exist in the group
	*/
	printf("Removing Krystal\n");
	retVal=remove_user(group,"Krystal");
	if(retVal==-1)
	{
		printf("User doesn't exist as expected\n");
	}
	else
	{
		printf("Unexpected return value = %d for removing nonexistant user\n",retVal);
		rVal=-1;
	}

	return rVal;
}

int test_find_prev_user()
{
	int rVal =0;
	Group* group=createGroup("GroupOne");
	add_user(group,"Sammy");
	add_user(group,"Celise");
	add_user(group,"Marina");
	add_user(group,"Alexandra");
	add_user(group,"Maegen");


	/*
		Tests when the user is somewhere in the middle,
		therefore the user does has a prev user
		available, which should be returned.

	*/
	User* prev = find_prev_user(group,"Marina");
	printf("Expecting Prev user = Alexandra\n");
	printf("User Returned=%s\n",prev->name);
	if(strcmp("Alexandra",prev->name)==0)
	{
		printf("Success,user = Alexandra returned.\n");
	}
	else
	{
		printf("Failure,user = Alexandra NOT returned\n");
		rVal=-1;
	}	
	
	prev=NULL;

	/*
		Tests when the user is at the start of the list
		and therefore has no previous user.
		This should return the user we passed in.
	*/
	prev=find_prev_user(group,"Maegen");
	printf("Expecting Prev user = Maegen\n");
	printf("User Returned=%s\n",prev->name);
	if(strcmp("Maegen",prev->name)==0)
	{
		printf("Success,user = Maegen returned.\n");
	}
	else
	{
		printf("Failure,user = Maegen NOT returned\n");
		rVal=-1;
	}	


	/*
		User searched for is not in the group,
		expecting NULL
	*/
	prev=find_prev_user(group,"Krystal");
	printf("Expecting Prev user = NULL\n");
	if(prev==NULL)
	{
		printf("Success,user = NULL returned.\n");
	}
	else
	{
		printf("Failure,user = NULL NOT returned\n");
		rVal=-1;
	}
	return rVal;	
}


int test_user_balance()
{
	Group* group;
	int rVal =0;
	int retVal=0;
	add_group(&group,"GroupOne");

	add_user(group,"Alexandra");
	add_user(group,"Sammy");
	add_user(group,"Maegen");

	User* usr = find_prev_user(group,"Alexandra")->next;
	usr->balance=123.45;

	printf("Getting Balance of user \"Alexandra\"\n");
	retVal = user_balance(group,"Alexandra");
	if(retVal!=0)
	{
		printf("Error. User \"Alexandra\" not found\n");
		rVal=-1;
	}
	else
	{
		printf("Success. Balance for User \"Alexandra\" retrieved\n");
	}


	printf("Getting Balance of non existant user \"Krystal\"\n");
	retVal = user_balance(group,"Krystal");
	if(retVal==0)
	{
		printf("Error. User \"Krystal\" was found, but shouldn't have.\n");
		rVal=-1;
	}
	else
	{
		printf("Success. User \"Krystal\" not found, as expected.\n");
	}
	


	return rVal;
}

int test_under_paid()
{
	
	Group* group;
	int retVal=0;
	add_group(&group,"GroupOne");

	add_user(group,"Alexandra");
	add_user(group,"Sammy");
	add_user(group,"Maegen");

	/*
		Test:Two users, Maegen and Sammy have paid the least.
	*/
	User* usr = find_prev_user(group,"Alexandra")->next;
	usr->balance=123.45;

	printf("Calling \"under_paid\". Expecting Sammy and Maegen\n");
	retVal=under_paid(group);
	if(retVal!=0)
	{
		printf("Error in call of \"under_paid\". List Empty.\n");
		return -1;
	}
	printf("After Calling \"under_paid\".Verify Output\n");

	/*
		Test: One user, Maegen, has paid the least.
	*/
	group->users->next->balance=100;/*Changing Sammy's balance*/

	printf("Calling \"under_paid\". Expecting Maegen Only\n");
	retVal=under_paid(group);
	if(retVal!=0)
	{
		printf("Error in call of \"under_paid\". List Empty.\n");
		return -1;
	}	
	printf("After Calling \"under_paid\".Verify Output\n");


	return 0;
}

int test_add_xct()
{
	Group* group=NULL;
	
	add_group(&group,"GroupOne");
	add_user(group,"lafanda");
	
	/*
		Test 1 - Add a user. Add a xct to them
		then 1)verify the transaction shows up on their
		user balance.
		2)Verify that the count of xct's goes up
	*/
	
	printf("Adding transaction\n");

	add_xct(group,"lafanda",12.03);

	printf("Testing transaction\n");
	User* usr = find_prev_user(group,"lafanda");		
	/* at this point there is only one user, so prev user is user*/

	
	if(strcmp(usr->name,"lafanda")!=0)
	{
		printf("Error:Something broke with get_prev_user.\n");
		printf("Only one user was added, and different one was returned\n");
		return -1;
	}	

	if(12.03!=usr->balance)
	{
		printf("Error:usr balance wasn't updated.\n");
		return -1;
	}
	if(group->xcts->amount!=12.03)
	{
		printf("Error:transaction wasn't added correctly to start of group list\n");
		return -1;
	}

	printf("Verify user ordering\n");
	list_users(group);	
	
	/*
		Test 2 - Add another user, create a bigger transaction
		Verify 1)transaction added to user balance. This user
			should be reordered to the end of the list.
			The users is at the start of the list to begin with
	*/
	printf("*************************\n");

	add_user(group,"mickey");
	printf("Adding another transaction\n");

	add_xct(group,"mickey",20.01);
	
	printf("Test transaction\n");
	usr=find_prev_user(group,"mickey")->next;
	if(strcmp(usr->name,"mickey")!=0) /*testing re-ordering*/
	{
		printf("Error: expected user mickey at the end of the list\n");
		return -1;
	}
	if(20.01!=usr->balance) /*testing balance update*/
	{
		printf("Error:usr mickey  balance wasn't updated.\n");
		return -1;
	}
	if(group->xcts->amount!=20.01)
	{
		printf("Error:transaction wasn't added correctly to start of group list for mickey\n");
		return -1;
	}	
	
	printf("Verify user ordering\n");
	list_users(group);	
	/*
		Test3 - Add another user, and then create a xct on
			the middle user (should be lafanda) to cause that
			user to move to the end of the list.
		verify) transaction added to user balance. This user 
		should be reordered to the end of the list.
		The user will be at the middle of the list to start with before
		adding a xct
	*/
	printf("*************************\n");
	add_user(group,"kitty");
	printf("Adding another transaction\n");
	add_xct(group,"lafanda",20.01);
	
	printf("Test transaction\n");
	usr=find_prev_user(group,"lafanda")->next;
	if(strcmp(usr->name,"lafanda")!=0) /*testing re-ordering*/
	{
		printf("Error: expected user lafanda at the end of the list\n");
		return -1;
	}
	if(32.04!=usr->balance) /*testing balance update*/
	{
		printf("Error:usr lafanda balance wasn't updated. Expected %lf got %lf\n",32.04,usr->balance);
		return -1;
	}
	if(group->xcts->amount!=20.01)
	{
		printf("Error:transaction wasn't added correctly to start of group list for lafanda. Expected %lf, got %lf\n",20.01,group->xcts->amount);
		return -1;
	}	
	printf("Verify user ordering\n");
	list_users(group);	
	

	/*
		Test4 - create a xct on	the end user (should be lafanda).
			User should not move. User ordering should all be the same.
		verify) transaction added to user balance. This user should be
		at the end still.
		The other users should be in the smae spot. 
	*/

	printf("*************************\n");
	printf("Verify user ordering\n");
	list_users(group);	
	add_xct(group,"lafanda",10.00);
	printf("Verify user ordering\n");
	printf("User ordering should not changei\n");
	list_users(group);	


	/*
		Test 5 - add a xct for a user that doesn't exist
			This should return -1;
	*/
	if(add_xct(group,"puppy",23.32)==0)
	{
		printf("Error:add_xct should've failed for nonexistant user\n");
		return -1;
	}
	return 0;
}

int test_recent_xct()
{
	Group* group=NULL;

	add_group(&group,"GroupOne");

	add_user(group,"Kitty");
	add_user(group,"puppy");
	add_user(group,"fishy");

	add_xct(group,"Kitty",10.10);
	add_xct(group,"puppy",22.22);
	add_xct(group,"puppy",11.11);
	add_xct(group,"fishy",5.55);
	add_xct(group,"Kitty",3.33);

	printf("Verify output. Only expecting %d entries\n",2);
	recent_xct(group,2);

	printf("Verify output. Only expecting %d entries\n",4);
	recent_xct(group,4);
	
	printf("Verify output. Only expecting max %d entries\n",10);
	recent_xct(group,10);


	return 0;
}

int test_remove_xct()
{
	Group* group=NULL;
	add_group(&group,"GroupOne");
	add_user(group,"Juanita");
	add_user(group,"Kitty");

	printf("Adding transactions...\n");
	add_xct(group,"Kitty",10.00);
	add_xct(group,"Juanita",11.00);
	add_xct(group,"Kitty",10.00);
	add_xct(group,"Juanita",11.00);

	Xct* xcts=NULL;
	int i=0;
	xcts=group->xcts;
	while(xcts!=NULL)
	{
		i++;
		xcts=xcts->next;
	}
	if(i!=4)
	{
		printf("Error:Expecting 4 transactions, %d were counted\n",i);
		return -1;
	}	
	printf("Removing transactions...\n");
	remove_xct(group,"Juanita");
	/*Based upon where transactions for Juanita are placed, this should
		test both main conditions in remove_xct*/
	i=0;
	xcts=group->xcts;
	while(xcts!=NULL)
	{
		i++;
		xcts=xcts->next;
	}
	if(i!=2)
	{
		printf("Error:Expecting 2 transactions, %d were counted\n",i);
		return -1;
	}	
	printf("Transaction removal verified\n");
	
	/*test 2: testing removal when the user doesn't exist*/
	printf("Testing removal of transactions of nonexistant user\n");
	remove_xct(group,"Puppy");
	i=0;
	xcts=group->xcts;
	while(xcts!=NULL)
	{
		i++;
		xcts=xcts->next;
	}
	if(i!=2)
	{
		printf("Error:Expecting 2 transactions, %d were counted\n",i);
		return -1;
	}	
	printf("Testing removal of transactions of nonexistant user verified\n");

	/*test3: testing removal of transactions from a group
		with no transactions */
	printf("Testing removal of nonexistant transactions from user\n");
	add_group(&group,"GroupTwo");
	add_user(group->next,"Puppy");
	if(group->next->xcts!=NULL)
	{
		printf("Error:Should be no transactions(before remove)\n");
		return -1;
	}

	printf("Removing nonexistant transaction now...\n");
	remove_xct(group->next,"Puppy");
	if(group->next->xcts!=NULL)
	{
		printf("Error:Should be no transactions(after remove)\n");
		return -1;
	}
	printf("Testing removal of nonexistant transactions from user verified\n");

	return 0;
}

int test_remove_user2()
{
	Group* group=NULL;
	add_group(&group,"GroupOne");
	add_user(group,"Kitty");
	add_user(group,"Puppy");
	add_user(group,"Birdy");

	add_xct(group,"Kitty",10.00);
	add_xct(group,"Puppy",11.11);
	add_xct(group,"Kitty",10.00);
	add_xct(group,"Kitty",10.00);
	add_xct(group,"Kitty",10.00);
	add_xct(group,"Kitty",10.00);
	add_xct(group,"Birdy",10.00);

	/*kitty should be last*/
	Xct* xct = group->xcts;
	int i=0;
	while(xct!=NULL)
	{
		if(strcmp(xct->name,"Kitty")==0)
		{
			i++;
		}
		xct=xct->next;
	}
	if(i!=5)
	{
		printf("Error:incorrect count of xcts for user %s. Expected %d, got %d\n","Kitty",5,i);
		return -1;
	}

	printf("Removing user %s\n","Kitty");
	
	remove_user(group,"Kitty");

	printf("Testing user is removed from group\n");
	User* usr = find_prev_user(group,"Kitty");

	if(usr!=NULL)
	{
		printf("Error:user %s not removed\n","Kitty");
		return -1;
	}

	printf("Tresting all transactions removed from xcts\n");
	xct = group->xcts;
	i=0;
	while(xct!=NULL)
	{
		if(strcmp(xct->name,"Kitty")==0)
		{
			i++;
		}
		xct=xct->next;
	}
	if(i!=0)
	{
		printf("Error:incorrect count of xcts for user %s. Expected %d, got %d\n","Kitty",0,i);
		return -1;
	}

	printf("User %s and all transactions removed \n","Kitty");	


	return 0;
}

int main(int argc,char* argv[])
{


	fprintf(stdout,"Running test_list_groups1()\n");
	test_list_groups1();

	fprintf(stdout,"=============================\n");
	fprintf(stdout,"Running test_find_group1()\n");
	if(test_find_group1()!=0)
	{
		printf("Test test_find_group1 failed! Examine the output\n");
		return -1;
	}
	
	fprintf(stdout,"=============================\n");
	fprintf(stdout,"Running test_add_group()\n");
	if(test_add_group()!=0)
	{
		printf("Test test_add_group failed! Examine the output\n");
		return -1;
	}
	
	fprintf(stdout,"=============================\n");
	fprintf(stdout,"Running testRemoveUser\n");
	if(testRemoveUser()!=0)
	{
		printf("Test testRemoveUser failed! Examine the output\n");
		return -1;
	}

	
	fprintf(stdout,"=============================\n");
	fprintf(stdout,"Running test_find_prev_user\n");
	if(test_find_prev_user()!=0)
	{
		printf("Test test_find_prev_user failed! Examine the output\n");
		return -1;
	}

	fprintf(stdout,"=============================\n");
	fprintf(stdout,"Running test_user_balance\n");
	if(test_user_balance()!=0)
	{
		printf("Test test_user_balance failed! Examine the output\n");
		return -1;
	}
	
	fprintf(stdout,"=============================\n");
	printf("Running test_under_paid\n");
	if(test_under_paid()!=0)
	{
		printf("Test test_inder_paid failed! Examine the output\n");
		return -1;
	}

	/*need to test remove_xct and additional ones for remove_user */

	fprintf(stdout,"=============================\n");
	printf("Running test_add_xct\n");
	if(test_add_xct()!=0)
	{
		printf("Test test_add_xct failed! Examine the output\n");
		return -1;
	}

	fprintf(stdout,"=============================\n");
	printf("Running test_recent_xct\n");
	if(test_recent_xct()!=0)
	{
		printf("Test test_recent_xct failed! Examine the output\n");
		return -1;
	}
	
	fprintf(stdout,"=============================\n");
	printf("Running test_remove_xct\n");
	if(test_remove_xct()!=0)
	{
		printf("Test test_remove_xct failed! Examine the output\n");
		return -1;
	}
	fprintf(stdout,"=============================\n");
	printf("Running test_remove_user2\n");
	if(test_remove_user2()!=0)
	{
		printf("Test test_remove_user2 failed! Examine the output\n");
		return -1;
	}


	printf("All tests passed\n");
	return 0;
}
