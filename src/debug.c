#include "debug.h"
#include <stdio.h>

#ifdef DEBUG

void debug(const char* message)
{
	fprintf(stdout,"Debug: %s\n",message);
}

#endif
