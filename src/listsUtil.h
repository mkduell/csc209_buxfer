#ifndef LISTS_UTIL_H
#define LISTS_UTIL_H

#include "lists.h"

Group* createGroup(const char* name);
void freeGroup(Group* grp);

User* createUser(const char* name);
void freeUser(User* usr);

Xct* createXct(const char* user_name,double amount);
void freeXct(Xct*);

void* myMalloc(size_t size);

#endif
