#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lists.h"
#include "listsUtil.h"
#ifdef DEBUG
	#include "debug.h"
#endif


/* Add a group with name group_name to the group_list referred to by 
* group_list_ptr. The groups are ordered by the time that the group was 
* added to the list with new groups added to the end of the list.
*
* Returns 0 on success and -1 if a group with this name already exists.
*
* (I.e, allocate and initialize a Group struct, and insert it
* into the group_list. Note that the head of the group list might change
* which is why the first argument is a double pointer.) 
*/
int add_group(Group **group_list_ptr, const char *group_name) 
{
	#ifdef DEBUG
		debug("Start:add_group\n");
	#endif

	Group* prev;
	Group* grp;
	if(*group_list_ptr!=NULL)
	{
		for(grp=*group_list_ptr;grp!=NULL;grp=grp->next)
		{
			if(strcmp(group_name,grp->name)==0)
			{
				#ifdef DEBUG
					debug("End:add_group:group exists\n");
				#endif
				return -1;
			}
			prev=grp;
		}

		#ifdef DEBUG
			debug("Adding group to end of the list\n");
		#endif
		/*adding to the end of the list*/
		prev->next=createGroup(group_name);
	}
	else /*group_list_ptr is NULL, set new group as the list*/
	{
		#ifdef DEBUG
			debug("Adding group as the group_list_ptr\n");
		#endif
		grp = createGroup(group_name);
		*group_list_ptr=grp;
	}

	#ifdef DEBUG
		debug("End:add_group:new group added\n");
	#endif

    return 0;
}

/* Print to standard output the names of all groups in group_list, one name
*  per line. Output is in the same order as group_list.
*/
void list_groups(Group *group_list) 
{
	#ifdef DEBUG
		debug("Start:list_groups\n");
	#endif

	Group* grp;
	for(grp=group_list;grp != NULL;grp=grp->next)
	{
		printf("%s\n",grp->name) ;
	}

	#ifdef DEBUG
		debug("End:list_groups\n");
	#endif
}

/* Search the list of groups for a group with matching group_name
* If group_name is not found, return NULL, otherwise return a pointer to the 
* matching group list node.
*/
Group *find_group(Group *group_list, const char *group_name) 
{
	#ifdef DEBUG
		debug("Start:find_group\n");
	#endif
	Group* cur_grp;
	for(cur_grp=group_list;cur_grp!=NULL;cur_grp=cur_grp->next)
	{
		if(strcmp(cur_grp->name,group_name)==0)
		{
			#ifdef DEBUG
				debug("End:find_group - Group found\n");
			#endif
			return cur_grp;
		}
	}

	#ifdef DEBUG
		debug("End:find_group - No group found\n");
	#endif
    return NULL;
}

/*
	Searches for the specific user.

	Returns NULL if user not found.
*/
User* find_user(Group* group,const char* user_name)
{
	#ifdef DEBUG
		debug("Start:find_user\n");
	#endif
	User* usr=find_prev_user(group,user_name);
	if(usr!=NULL)/*User exists*/
	{
		/*Two Cases. 
		  1)prev is the user we are looking for (start of the list)
		  In which case we already have the user we need
			2)prev is the previous user of the user we are looking for
		*/
		/*case2*/
		/*user isn't the one we searched for => usr->next = one we want 
			don't have to explicitly take care of case1
		*/
		if(strcmp(usr->name,user_name)!=0) 
		{
			usr=usr->next;
		}
		#ifdef DEBUG
			debug("End:find_user - User found\n");
		#endif
		return usr;
	}
	#ifdef DEBUG
		debug("End:find_user - User not found\n");
	#endif
	
	return NULL;
}

/* Add a new user with the specified user name to the specified group. Return zero
* on success and -1 if the group already has a user with that name.
* (allocate and initialize a User data structure and insert it into the
* appropriate group list)
*/
int add_user(Group *group, const char *user_name) 
{
	#ifdef DEBUG
		debug("Start:add_user\n");
	#endif
	User* usr;
	usr = find_prev_user(group,user_name);
	if(usr==NULL)
	{
		usr = createUser(user_name);
		/*adding to front since it will by default have the lowest balance*/
		usr->next=group->users;
		group->users=usr;	
	
		#ifdef DEBUG
			debug("End:add_user:user added\n");
		#endif
    	return 0;
	}
	else
	{
		#ifdef DEBUG
			debug("End:add_user:user already exists\n");
		#endif
		return -1;
	}
	
	
}

/* Remove the user with matching user and group name and
* remove all her transactions from the transaction list. 
* Return 0 on success, and -1 if no matching user exists.
* Remember to free memory no longer needed.
* (Wait on implementing the removal of the user's transactions until you 
* get to Part III below, when you will implement transactions.)
*/
int remove_user(Group *group, const char *user_name) {

	#ifdef DEBUG
		debug("Start:remove_user\n");
	#endif

	User* usr=NULL;
	User* prev=find_prev_user(group,user_name);

	if(prev!=NULL)/*User exists, it can be removed*/
	{
		/*Two Cases. 
			1)prev is the user we are looking for (start of the list)
			2)prev is the previous user of the user we are looking for
		*/
		/*case1*/
		if(strcmp(prev->name,user_name)==0)
		{
			/*adjust the head of the list*/
			group->users=prev->next;
			freeUser(prev);
		}
		else /*case2*/
		{
			usr=prev->next;
			prev->next=usr->next;
			freeUser(usr);
		}
		/*
			remove the transactions for the specific user
		*/
		remove_xct(group,user_name);

		#ifdef DEBUG
			debug("End:remove_user:successfully removed user\n");
		#endif
		return 0;
	}
	else /*User does not exist in this group*/
	{
		#ifdef DEBUG
			debug("End:remove_user:User does not exist in group");
		#endif
		return -1;
	}
}

/* Print to standard output the names of all the users in group, one
* per line, and in the order that users are stored in the list, namely 
* lowest payer first.
*/
void list_users(Group *group) 
{
	#ifdef DEBUG
		debug("Start:list_users\n");
	#endif
	User* usr;
	for(usr=group->users;usr!=NULL;usr=usr->next)
	{
		printf("%s\n",usr->name);
	}
	#ifdef DEBUG
		debug("End:list_users\n");
	#endif
}

/* Print to standard output the balance of the specified user. Return 0
* on success, or -1 if the user with the given name is not in the group.
*/
int user_balance(Group *group, const char *user_name) {
	User* usr=find_prev_user(group,user_name);

	if(usr!=NULL)/*User exists*/
	{
		/*Two Cases. 
		  1)prev is the user we are looking for (start of the list)
		  In which case we already have the user we need
			2)prev is the previous user of the user we are looking for
		*/
		/*case2*/
		/*user isn't the one we searched for => usr->next = one we want 
			don't have to explicitly take care of case1
		*/
		if(strcmp(usr->name,user_name)!=0) 
		{
			usr=usr->next;
		}
		printf("%.2lf\n",usr->balance);
		return 0;
	}
    return -1;
}

/* Print to standard output the name of the user who has paid the least 
* If there are several users with equal least amounts, all names are output. 
* Returns 0 on success, and -1 if the list of users is empty.
* (This should be easy, since your list is sorted by balance). 
*/
int under_paid(Group *group) {
    
	User* usr;
	if(group!=NULL)
	{
		if(group->users!=NULL)
		{
			/*keep advancing until a user has paid more than the first users 
				or end of list*/
			for(usr=group->users;usr!=NULL &&(usr->balance <= group->users->balance);usr=usr->next)
			{
				printf("%s\n",usr->name);
			}
			return 0;
		}		
		return -1;
	}

	return -1;
}

/* Return a pointer to the user prior to the one in group with user_name. If 
* the matching user is the first in the list (i.e. there is no prior user in 
* the list), return a pointer to the matching user itself. If no matching user 
* exists, return NULL. 
*
* The reason for returning the prior user is that returning the matching user 
* itself does not allow us to change the user that occurs before the
* matching user, and some of the functions you will implement require that
* we be able to do this.
*/
User *find_prev_user(Group *group, const char *user_name) {
    
	User* usr;
	if(group->users==NULL)
	{
		return NULL;
	}
	for(usr=group->users;usr!=NULL;usr=usr->next)
	{
		/*
			Check what the value of the next one is,
			and if it matches, return current.
		*/
		if(usr->next!=NULL)
		{
			if(strcmp(usr->next->name,user_name)==0)
			{
				return usr;
			}
		}
		/*
			This will cover the case where the desired
			user is the first one.
		*/
		if(strcmp(usr->name,user_name)==0)
		{
			return usr;
		}
	}
	return NULL;
}

void updateUserOrdering(Group* group,User* usr)
{
	#ifdef DEBUG
		debug("Start:updateUserOrdering\n");
	#endif
	User* next;
	while(usr->next!=NULL)
	{
		next=usr->next;
		if(usr->balance > next->balance)
		{
			usr->next=next->next;
			
			/*usr is not at the front of the list */
			if(strcmp(find_prev_user(group,usr->name)->name,usr->name)!=0)
			{
				find_prev_user(group,usr->name)->next=next;
			}
			else /*usr is at front of the list, move the user it just
				passed by to the front of the list. */
			{
				group->users=next;
			}

			next->next=usr;
		}
		else
		{
			break;
		}		
	}
	#ifdef DEBUG
		debug("End:updateUserOrdering\n");
	#endif
}

/* Add the transaction represented by user_name and amount to the appropriate 
* transaction list, and update the balances of the corresponding user and group. 
* Note that updating a user's balance might require the user to be moved to a
* different position in the list to keep the list in sorted order. Returns 0 on
* success, and -1 if the specified user does not exist.
*/
int add_xct(Group *group, const char *user_name, double amount) {
   
	#ifdef DEBUG
		debug("Start:add_xct\n");
	#endif 

	User* usr;
	usr = find_user(group,user_name);
	if(usr!=NULL)
	{
		/* Add xct to group at start of xct list */
		Xct* xct=createXct(user_name,amount);
		xct->next=group->xcts;
		group->xcts=xct;
		/* modify the user's running balance in group->users */
		usr->balance+=amount;
		/* change the users location in the list if necessary */
		updateUserOrdering(group,usr);

		#ifdef DEBUG
			debug("End:add_xct:Transaction added.\n");
		#endif 
		return 0; 
	}	

	#ifdef DEBUG
		debug("End:add_xct:User not found.Transaction not added.\n");
	#endif 

	return -1;
}

/* Print to standard output the num_xct most recent transactions for the 
* specified group (or fewer transactions if there are less than num_xct 
* transactions posted for this group). The output should have one line per 
* transaction that prints the name and the amount of the transaction. If 
* there are no transactions, this function will print nothing.
*/
void recent_xct(Group *group, long nu_xct) {
	#ifdef DEBUG
		debug("Start:recent_xct\n");
	#endif

	if(group!=NULL)
	{
		Xct* xct=group->xcts;
		int i=0;
		while(xct!=NULL && i<nu_xct)
		{
			printf("%s : %.2lf\n",xct->name,xct->amount);
			xct=xct->next;
			i++;
		}
	}
	#ifdef DEBUG
		debug("End:recent_xct\n");
	#endif
}


/* Remove all transactions that belong to the user_name from the group's 
* transaction list. This helper function should be called by remove_user. 
* If there are no transactions for this user, the function should do nothing.
* Remember to free memory no longer needed.
*/

void remove_xct(Group *group, const char *user_name) {

	#ifdef DEBUG
		debug("Start:remove_xct\n");
	#endif
	if(group!=NULL)
	{
		Xct* cur=group->xcts;
		Xct* prev=NULL;

		while(cur!=NULL)
		{
			/*Found an item to delete*/
			if(strcmp(cur->name,user_name)==0)
			{
				/*the transaction examined is in the middle
					somewhere */
				if(prev!=NULL)
				{
					prev->next=cur->next;
					freeXct(cur);
					cur=prev;
				}
				else/*implies we are at the start of the list*/
				{
					group->xcts=cur->next;
					freeXct(cur);
					cur=group->xcts;
				}
			}
			else/*advance to the next item in the list*/
			{
				prev=cur;
				cur=cur->next;
			}
		}
	}

	#ifdef DEBUG
		debug("End:remove_xct\n");
	#endif
}

